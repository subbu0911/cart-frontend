import React from 'react';

import axios from 'axios';

class Example extends React.Component {
  state = {
    persons: []
  }

  componentDidMount() {
    axios.get('http://127.0.0.1:8000/api/products/')
      .then(res => {
        const persons = res.data;
        this.setState({ persons });
        console.log(res.data);
      })
  }

  render() {
    return (
      <ul>
        { this.state.persons.map(person => <li>{person.name}</li>)}
      </ul>
    )
  }
}

export default Example;