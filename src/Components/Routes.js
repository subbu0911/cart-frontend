import React from 'react';
import { BrowserRouter as Router,Switch,Route,Link} from 'react-router-dom';

import Products from './Products';
import Login from './Login';
import Register from './Register';
import Cart from './Cart';


const Routes = () =>{

    return(
        <Router>
            <div>
            <nav class="navbar navbar-expand-lg navbar-dark bg-dark container-fluid">
            <a class="navbar-brand" href="#"><Link to='/Products'><span class="badge">Amazon</span></Link></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
                <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                <li class="nav-item active">
                    <a class="nav-link" href="#"><Link to='/Products'>Products</Link></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#"><Link to='/Login'>Sign In</Link></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#"><Link to='/Register'>Sign Up</Link></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#"><Link to='/Cart'>Cart</Link></a>
                </li>
                
                </ul>
                <form class="form-inline my-2 my-lg-0">
                <input class="form-control mr-sm-2" type="search" placeholder="Search"></input>
                <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                </form>
                </div>
               
                </nav>

                

                    
                    
                    <Switch>
                    <Route path="/Login"><Login /></Route> 
                    
                    <Route path="/Register"><Register /></Route>
                    <Route path="/Cart"><Cart /></Route>
                    <Route path="/"><Products /></Route> 
                    

                
                    {/* <Route path="/*"><NotFound></NotFound></Route> */}
                    <Route component={() => (<div>404 Not found </div>)} />
                    </Switch>
               
            </div>
            
        </Router>
    );
}



export default Routes;