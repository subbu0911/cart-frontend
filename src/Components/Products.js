import React from 'react';
import axios from 'axios';
import 'bootstrap-css-only/css/bootstrap.min.css';

class Products extends React.Component {
  constructor() {
    super();
    this.state = {
      rest: []
    }
  }
  componentDidMount() {
    console.log("componentDidMount");

    axios.get('http://127.0.0.1:8000/api/products/').then(res => {
      const rest = res.data;
      this.setState({ rest });
      console.log(res.data)
    });


  }
  render() {

    return (
      <div className="container">
        <br></br>
        <br></br>
        <div className="row">
          
          
          {this.state.rest.map((item, index) => (
            
            <div className="col-md-4">
            <div className="card" style={{marginBottom:"30px"}} >
                
              <img className="card-img-top" src={item.image} alt="Card image cap" style={{ height: '500px',width: 'auto'}} ></img>
              <div className="card-body">
               <h1 className="card-text text-dark " style={{fontSize:"22px",fontWeight:"700"}}>{item.title}</h1>
                <p className="card-text display-2 ">{item.description}</p>
                
                <a href="#" className="btn btn-warning">Buy Now</a>
                <span className="lead"><span className="badge badge-primary float-right">${item.price}</span></span>
              </div>
            </div>
            </div>
          ))}
        </div>
      </div>

    );
  }
}
export default Products;

