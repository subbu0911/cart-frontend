import React, { useState, useEffect} from 'react';
import {Col} from 'react-bootstrap';

const Cart = () =>{
const [orderitems,setOrderitems] = useState([]);
useEffect( () => {
    fetch('http://localhost:8000/api/order_items/',{
        method: 'GET',
        headers : {
            'content-Type': 'application/json',
            'Authorization':'Token d4d4fbc4676732a12004a93661fc2d9ed9a611a7'
        }
    })
    .then(resp => resp.json())
    .then(resp => setOrderitems(resp))
    .catch(error => console.log(error))
    // console.log(resp.json())

},[])
return (
    <div className="container">
        <br></br>
        <br></br>
        <h1 className="display-4 text text-dark text-center bg bg-info">My Cart</h1>
        <br></br>
        <div className="row">
          
          
        {orderitems.map(order=> (
            
            <div className="col-md-4" key={order.id}>
            <div className="card" style={{marginBottom:"30px"}} >
                
              <img className="card-img-top" src={order.product_id.image} alt="Card image cap" style={{ height: '500px',width: 'auto'}} ></img>
              <div className="card-body">
               <h1 className="card-text text-dark text-center " style={{fontSize:"22px",fontWeight:"700"}}>{order.product_id.title}</h1>
               <p className="card-text display-1 " style={{fontSize:"18px",fontWeight:"500"}}>qty:{order.quantity}</p>
               <p className="card-text display-1 " style={{fontSize:"18px",fontWeight:"500"}}>Price:${order.price}  </p>

                <span className="lead"><span className="badge badge-primary float-right">Total:${order.final_price}</span></span>
              </div>
            </div>
            </div>
          ))}
        </div>
      </div>

    );
  }

   
export default Cart;


