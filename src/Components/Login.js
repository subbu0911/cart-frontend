import React, { Component} from 'react';


class Login extends Component {

  state = {
    credentials: {username: '', password: ''}
  }

  login = event => {
    fetch('http://127.0.0.1:8000/api/login/', {
      method: 'POST',
      headers: {'Content-Type': 'application/json'},
      body: JSON.stringify(this.state.credentials)
    })
    .then( data => data.json(),
      
    )
    .then(
      data => {
        this.props.userLogin(data.token);
      }
    )
    .catch( error => console.error(error))
  }

  
  inputChanged = event => {
    const cred = this.state.credentials;
    cred[event.target.name] = event.target.value;
    this.setState({credentials: cred});
  }

  render() {
    return (
      <div className=" justify-content-center">
        <br></br>
        <br></br>
      <div className="card" >
        
        
        <h1 className="text text-dark text-center">Sign In
        Here</h1>
        <hr></hr>
        <div className="text-center">
        <label style={{color:'black'}}>
          Username:
          <input type="text" name="username"
           value={this.state.credentials.username}
           onChange={this.inputChanged}/>
        </label>
        <br/>
        <br />
        <label style={{color:'black'}}>
          Password:
          <input type="password" name="password"
           value={this.state.credentials.password}
           onChange={this.inputChanged} />
        </label>
        <br/>
        <br></br>
        <span><button className="btn btn-success" onClick={this.login}>Login</button></span>
        <span><button className="btn btn-secondary" onClick={this.register}>Register</button></span>
        </div>
      </div>
      </div>
    );
  }
}

export default Login;